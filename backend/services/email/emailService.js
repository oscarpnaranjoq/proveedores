const nodemailer = require("nodemailer");
const db = require("../../models/DB");
const { QueryTypes } = require('sequelize');

async function enviarCorreo(opcionesCorreo) {
    const credenciales = await db.sequelize.query(`SELECT * FROM v_mqry_credencialcorreos`,
        {
            type: QueryTypes.SELECT,
            plain: true
        }
    ).then(result => {
        return result
    });
    let configTransporte = {
        host: "smtp.office365.com",
        port: 587,
        secure: false,
        auth: {
            user: credenciales.BUSONMAIL,
            pass: credenciales.PASSBUSMAIL
        }
    };
    return new Promise((resolve, reject) => {
        let transporter = nodemailer.createTransport(configTransporte);
        transporter.sendMail(opcionesCorreo, (error, info) => {
            if (error) {
                reject(error);
            } else {
                resolve(info);
            }
        });
    });
}

module.exports = { enviarCorreo }