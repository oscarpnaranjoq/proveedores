const db = require("../../models/DB");
const { QueryTypes } = require('sequelize');
const moment = require('moment');

async function findFacturasByParams(data) {
    const { identificacion } = data;
    const sql = `SELECT * FROM v_mqry_cxp_pagos_ejecutados WHERE CEDULA_O_RUC='${identificacion}' ${validacionCondicion(data)} ORDER BY FECHA_FACTURA DESC ${validacionCondicion(data).length > 0 ? '' : 'FETCH FIRST 30 ROWS ONLY'}`;
    const sqlEmpresas = `SELECT COD_EMPRESA,NOMBRE_EMPRESA FROM v_mqry_cxp_pagos_ejecutados WHERE CEDULA_O_RUC='${identificacion}' GROUP BY COD_EMPRESA,NOMBRE_EMPRESA`;
    const facturas = await db.sequelize.query(sql,
        {
            type: QueryTypes.SELECT,
            plain: false
        }
    ).then(result => {
        return result
    });
    const empresas = await db.sequelize.query(sqlEmpresas,
        {
            type: QueryTypes.SELECT,
            plain: false
        }
    ).then(result => {
        return result
    });
    const result = {
        facturas: facturas,
        empresas: empresas
    }
    return result;
}

function validacionCondicion(data) {
    const { fechaInicio, fechaFinal, sEmpresa, numFactura } = data;
    let condicion;
    if (fechaInicio && fechaFinal && sEmpresa) {
        condicion = ` AND FECHA_FACTURA BETWEEN '${moment(fechaInicio).format('DD/MM/YYYY')}' AND '${moment(fechaFinal).format('DD/MM/YYYY')}' AND COD_EMPRESA='${sEmpresa}'`;
    } else if (fechaInicio && fechaFinal && fechaInicio !== "Invalid date" && fechaFinal !== "Invalid date") {
        condicion = ` AND FECHA_FACTURA BETWEEN '${moment(fechaInicio).format('DD/MM/YYYY')}' AND '${moment(fechaFinal).format('DD/MM/YYYY')}'`;
    } else if (fechaInicio && fechaInicio !== "Invalid date") {
        condicion = ` AND FECHA_FACTURA='${moment(fechaInicio).format('DD/MM/YYYY')}'`;
    } else if (fechaFinal && fechaFinal !== "Invalid date") {
        condicion = ` AND FECHA_FACTURA='${moment(fechaFinal).format('DD/MM/YYYY')}'`;
    } else if (sEmpresa) {
        condicion = ` AND COD_EMPRESA='${sEmpresa}'`;
    } else if (numFactura) {
        condicion = ` AND NUM_FACTURA='${numFactura}'`;
    }
    return condicion === undefined ? "" : condicion;
}
module.exports = { findFacturasByParams }