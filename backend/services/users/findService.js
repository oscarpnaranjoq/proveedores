const db = require("../../models/DB");
const { QueryTypes } = require('sequelize');
const { map } = require("sequelize-oracle/lib/promise");
const dbSian = require('../../models/DB/sianDBModel')

async function findByUserName(data) {
    if (!data.username) throw new Error('Empleado no existe');
    return await db.sequelize.query(
        `
        SELECT t.*,
        (
            SELECT RTRIM(XMLAGG(XMLELEMENT(e,t1.eaemal || ',')).EXTRACT('//text()'),',') 
                    FROM (SELECT to_char(f.eaemal) eaemal FROM proddta.f01151 f WHERE f.eaan8=(select COD_PROVEEDOR from v_qry_cxp_pagos_ejecutados WHERE CEDULA_O_RUC= :username GROUP BY COD_PROVEEDOR)
            UNION
            SELECT u.usu_correo eaemal FROM qry_jdp.wp_usuario u WHERE u.USU_RUC = :username) t1
        ) EMAILS,
        (select COD_PROVEEDOR from v_qry_cxp_pagos_ejecutados WHERE CEDULA_O_RUC= :username GROUP BY COD_PROVEEDOR) COD_PROVEEDOR  FROM qry_jdp.wp_usuario t WHERE t.USU_RUC = :username AND t.USU_PSW = :clave
        `,
        {
            replacements: { username: data.username, clave: data.clave },
            type: QueryTypes.SELECT,
            plain: true
        }
    ).then(async result => {
        const data = await getMenu();
        result.MENU = data.menu;
        result.ROUTES = data.routes;
        return result
    });
}
async function findByUsuario(data) {
    if (!data.username) throw new Error('Empleado no existe');
    return await db.sequelize.query(
        `
        SELECT t.*,
        (
            SELECT RTRIM(XMLAGG(XMLELEMENT(e,t1.eaemal || ',')).EXTRACT('//text()'),',') 
                    FROM (SELECT to_char(f.eaemal) eaemal FROM proddta.f01151 f WHERE f.eaan8=(select COD_PROVEEDOR from v_qry_cxp_pagos_ejecutados WHERE CEDULA_O_RUC= :username GROUP BY COD_PROVEEDOR)
            UNION
            SELECT u.usu_correo eaemal FROM qry_jdp.wp_usuario u WHERE u.USU_RUC = :username) t1
        ) EMAILS,
        (select COD_PROVEEDOR from v_qry_cxp_pagos_ejecutados WHERE CEDULA_O_RUC= :username GROUP BY COD_PROVEEDOR) COD_PROVEEDOR  FROM qry_jdp.wp_usuario t WHERE t.USU_RUC = :username
        `,
        {
            replacements: { username: data.username },
            type: QueryTypes.SELECT,
            plain: true
        }
    ).then(async result => {
        const data = await getMenu();
        result.MENU = data.menu;
        result.ROUTES = data.routes;
        return result
    });
}

const getMenu = async () => {
    const sql = `select * from sian.gc_menu where men_id>=500 and men_nivel=1`;
    return await dbSian.sequelize.query(sql,
        {
            type: QueryTypes.SELECT,
            plain: false
        }
    ).then(async result => {
        const menu = [];
        for (const e of result) {
            menu.push({
                id: e.MEN_ID.toString(),
                title: e.MEN_DESCRIPCION,
                type: e.MEN_TYPE,
                icon: e.MEN_ICON,
                children: await getSubMenu(e.MEN_ID)
            })
        }
        var ids = `SELECT m.men_app path FROM sian.gc_menu m where m.men_padre in (select men_id from sian.gc_menu where men_id>=500 and men_nivel=1)`;
        const routes = await dbSian.sequelize.query(ids,
            {
                type: QueryTypes.SELECT,
                plain: false
            }
        ).then(async result => {
            let route = [];
            for (const e of result) {
                route.push({
                    path: e.PATH,
                })
            }
            return route;
        });

        const data = { menu: menu, routes: routes }
        return data;
    });
}

const getSubMenu = async (idMenu) => {
    const sql = `select * from sian.gc_menu where men_padre=${idMenu} and men_url!=' '`;
    return await dbSian.sequelize.query(sql,
        {
            type: QueryTypes.SELECT,
            plain: false
        }
    ).then(result => {
        let subMenu = [];
        result.forEach(e => {
            subMenu.push({
                id: e.MEN_ID.toString(),
                title: e.MEN_DESCRIPCION,
                translate: e.MEN_TRASLATE,
                type: e.MEN_TYPE,
                icon: e.MEN_ICON,
                url: e.MEN_URL,
                app: e.MEN_APP
            })
        });
        return subMenu
    });
}

module.exports = { findByUserName, findByUsuario }