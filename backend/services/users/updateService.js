const db = require("../../models/DB");
const { QueryTypes } = require('sequelize');
const { _sendEmail } = require("../../controllers/emailController");
const { htmlEmailResetPasswordTemplate } = require("../../templates/emailResetPasswordTemplate");

async function updateCreateInfo(data) {
    try {
        const { password, passwordConfirm, emails } = data;
        if ((password === passwordConfirm) && (password && passwordConfirm) && (emails.length > 0)) {
            updatePassword(data, 0);
            insertEmails(data);
        } else if ((password === passwordConfirm) && (password && passwordConfirm)) {
            updatePassword(data, 0);
        } else if (emails.length > 0) {
            insertEmails(data);
        }
        return "Transacción Exitosa";
    } catch (e) {
        return Error(e)
    }

}


async function updatePassword(data, email) {
    try {
        const { userId, password, emails } = data;
        const sqlUpdatePWD = `UPDATE qry_jdp.wp_usuario SET USU_PSW = :password WHERE USU_RUC = :identificacion`;
        const updatePWD = await db.sequelize.query(sqlUpdatePWD,
            {
                replacements: { password: password, identificacion: userId },
                type: QueryTypes.UPDATE,
                returning: true,
                plain: true
            }
        ).then(result => {
            return result[0] === undefined ? 1 : result;
        });
        if (updatePWD === 1 && email === 1) {
            let opcionesCorreo = {
                from: "carteraproveedores@danec.com",
                to: emails,
                subject: "Nueva contraseña",
                html: htmlEmailResetPasswordTemplate(password)
            };
            _sendEmail(opcionesCorreo).then((info) => {
                return 200;
            }).catch((error) => {
                throw new Error("Error al enviar correo: ", error)
            });
            return "Transacción Exitosa"
        } else {
            return new Error("Error en microservicio updateService->updatePasswprd")
        }
        // return
    } catch (e) {
        return Error(e)
    }
}

async function insertEmails(data) {
    try {
        const { codUsuario, emails } = data;
        emails.forEach(e => {
            db.sequelize.query(`INSERT INTO proddta.f01151 (EAAN8,EAIDLN,EARCK7,EAETP,EAEMAL,EAUSER,EAPID,EAUPMJ,EAJOBN,EAUPMT,EAEHIER,EAEFOR,EAECLASS,EACFNO1,EAGEN1,EAFALGE,EASYNCS,EACAAD) 
             VALUES (:codProveedor,0,(select NVL(MAX(f.earck7),0)+1 from proddta.f01151 f where f.eaan8=:codProveedor),'E   ',:correo,'WEBPROV','P01111',
             (SELECT TO_NUMBER(TO_CHAR(SYSDATE, 'YYYYDDD'))-1900000 FROM DUAL),'srvapp01',
             (SELECT TO_CHAR(SYSDATE,'HH24') || TO_CHAR(SYSDATE,'MI') || TO_CHAR(SYSDATE,'SS') FROM DUAL),1,' ','E  ',0,' ',' ',0,0)`,
                {
                    replacements: {
                        codProveedor: codUsuario,
                        correo: e
                    },
                    type: QueryTypes.INSERT,
                    returning: true,
                    plain: true
                })
        });
    } catch (e) {
        return Error(e)
    }
}

module.exports = { updateCreateInfo, updatePassword }