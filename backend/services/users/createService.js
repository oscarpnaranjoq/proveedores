const bcrypt = require('bcrypt');
const db = require('../../models/DB');
const { QueryTypes } = require('sequelize');
const { _sendEmail } = require('../../controllers/emailController');
const { htmlEmailCreateUserTemplate } = require('../../templates/emailCreateUserTemplate');

async function create(user) {
    const { username, password, email } = user;
    const regex = /^\d{13}$/;
    const regexEmail = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    if (regex.test(username) === false) throw new Error('Ruc debe contener 13 digitos');
    if (regexEmail.test(email) === false) throw new Error('Correo invalido');
    if (!username) throw new Error('Ingrese usuario');
    if (!email) throw new Error('Ingrese email');

    const res = await db.sequelize.query(`INSERT INTO qry_jdp.wp_usuario (USU_RUC,USU_PSW,USU_EST,USU_ROL,USU_CLAVESTATUS,USU_CORREO) 
    VALUES(:username,:password,1,1,1,:email)`,
        {
            replacements: {
                username: username,
                password: password,
                email: email
            },
            type: QueryTypes.INSERT
        }
    ).then(result => {
        return result[0] === undefined ? 1 : result;
    });
    if (res === 1) {
        let opcionesCorreo = {
            from: "carteraproveedores@danec.com",
            to: email,
            subject: "Creación Usuario",
            html: htmlEmailCreateUserTemplate(username, password)
        };
        _sendEmail(opcionesCorreo).then((info) => {
            return 200;
        }).catch((error) => {
            throw new Error("Error al enviar correo: ", error)
        });
        return "Transacción Exitosa, revise su correo."
    } else {
        return new Error("Error en microservicio sing-up->create")
    }
}
module.exports = { create }