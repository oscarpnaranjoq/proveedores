function generatePWD() {
    const caracteres = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    let pwd = "";
    for (let i = 0; i < 8; i++) {
        pwd += caracteres.charAt(Math.floor(Math.random() * caracteres.length));
    }
    return pwd;
}

module.exports = { generatePWD }