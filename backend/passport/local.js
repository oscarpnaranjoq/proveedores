const LocalStrategy = require('passport-local').Strategy;
const { _findByUserName } = require('../controllers/userController');
const bcrypt = require('bcrypt');

module.exports = new LocalStrategy({
    usernameField: 'username',
    passwordField: 'clave',
    passReqToCallback: true,
    session: false
}, async (req, username, password, done) => {
    try {
        const user = await _findByUserName(req.body);
        if (!user) return done(null, false, 'Usuario o contraseña incorrectos');
        // const match = bcrypt.compareSync(password, user.clave);
        if (password !== user.USU_PSW) return done(null, false, 'Usuario o contraseña incorrectos');
        return done(null, {
            usuario: user.USU_RUC,
            username: user.USU_RUC,
            pwd: user.USU_PSW,
            emails: user.EMAILS,
            codUsuario: user.COD_PROVEEDOR,
            id: user.USU_RUC,
            menu: user.MENU,
            routes: user.ROUTES,
        });
    } catch (e) {
        done(e);
    }
});