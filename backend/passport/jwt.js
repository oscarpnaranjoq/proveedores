const JWTStrategy = require('passport-jwt').Strategy,
    ExtractJWT = require('passport-jwt').ExtractJwt,
    { _findByUsuario } = require('../controllers/userController');

module.exports = new JWTStrategy({
    jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
    secretOrKey: process.env.SECRET_KEY,
    ignoreExpiration: false
}, async (jwt_payload, done) => {
    try {
        const user = await _findByUsuario(jwt_payload);
        if (!user) return done(null, false, 'No autorizado');
        return done(null, {
            usuario: user.USU_RUC,
            username: user.USU_RUC,
            pwd: user.USU_PSW,
            emails: user.EMAILS,
            codUsuario: user.COD_PROVEEDOR,
            id: user.USU_RUC,
            menu: user.MENU,
            routes: user.ROUTES,
        });
    } catch (e) {
        done(e)
    }
});