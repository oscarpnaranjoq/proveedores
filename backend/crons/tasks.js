const exec = require('child_process').exec;
const cron = require('node-cron');
const process = require("process");
const fs = require("fs");
const os = require("os");
// cron.schedule('* * * * *', () => {
//     console.log('running a task every minute');
// });
// async function execSDM() {
//     cron.schedule('* * * * *', () => {

//         console.log('running a task every minute');

//     });
// }
// const childPorcess = exec('java -jar C:\\Users\\egamboy\\Documents\\NetBeansProjects\\ExportarCSV\\dist\\ExportarCSV.jar "Jar is invoked by Node js"', function (err, stdout, stderr) {
//     if (err) {
//         console.log(err)
//     }
//     console.log(stdout)
// })

const execExportTemplatesCSV = cron.schedule('29 11 * * *', () => {
    registrarLog(`=====Tarea iniciada GENERAR TEMPLATES=> a las ${new Date()}`);
    exec('java -jar C:\\Users\\egamboy\\Documents\\NetBeansProjects\\ExportarCSV\\dist\\generarTemplates.jar "Jar is invoked by Node js"', function (err, stdout, stderr) {
        if (err) {
            registrarLog(`ERROR!!!! GENERAR TEMPLATES=> ${err}`);
        } else {
            // registrarLog(`RESPUESTA => ${stdout}`);
            registrarLog(`=====Tarea finalizada GENERAR TEMPLATES=> a las ${new Date()}`);
            setTimeout(() => {
                execCMDReplaceUpdate();
            }, 100000)

        }
    });
});
function execCMDReplaceUpdate() {
    registrarLog(`=====Tarea iniciada CDM REPLACEUPDATE=> a las ${new Date()}`);
    exec('java -jar C:\\Users\\egamboy\\Documents\\NetBeansProjects\\ExportarCSV\\dist\\ExportarCSV.jar "Jar is invoked by Node js"', function (err, stdout, stderr) {
        if (err) {
            registrarLog(`ERROR!!!! CDM REPLACEUPDATE=> ${err}`);
        } else {
            // registrarLog(`RESPUESTA => ${stdout}`);
            registrarLog(`=====Tarea finalizada CDM REPLACEUPDATE=> a las ${new Date()}`);
            setTimeout(() => {
                execCMDReplaceUpdate();
            }, 500000)

        }
    })
}
function execCMDReplaceUpdate() {

}
function registrarLog(text) {
    const freeMemory = Math.round((os.freemem() * 100) / os.totalmem()) + "%";
    let csv = `${text}, ${freeMemory}=====\n`;
    fs.appendFile("cronJobs.log", csv, function (err) {
        if (err) throw err;
    });
}
module.exports = { execExportTemplatesCSV }