const { findOrdenesById, findDetailOrdenesById } = require("../services/ordenes/findService");

async function _findOrdenesById(req) {
    return await findOrdenesById(req);
}
async function _findDetailOrdenesById(req) {
    return await findDetailOrdenesById(req);
}
module.exports = { _findOrdenesById, _findDetailOrdenesById }