const { create } = require("../services/users/createService");
const { findByUserName, findByUsuario, findAll } = require("../services/users/findService");
const { updateCreateInfo, updatePassword } = require("../services/users/updateService");

async function _create(user) {
    return await create(user);
}

async function _findByUserName(data) {
    return await findByUserName(data);
}
async function _findByUsuario(data) {
    return await findByUsuario(data);
}

async function _findAll() {
    return await findAll();
}

async function _updateCreateInfo(req) {
    return await updateCreateInfo(req);
}

async function _resetPassword(req) {
    return await updatePassword(req, 1);
}
module.exports = { _create, _findByUserName, _findByUsuario, _findAll, _updateCreateInfo, _resetPassword }