const { enviarCorreo } = require("../services/email/emailService");

async function _sendEmail(req) {
    return await enviarCorreo(req);
}

module.exports = { _sendEmail }