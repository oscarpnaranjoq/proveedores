const { findFacturasByParams } = require("../services/facturas/findService");

async function _findFacturasByParams(req) {
    return await findFacturasByParams(req);
}

module.exports = { _findFacturasByParams }