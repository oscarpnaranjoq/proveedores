'use strict';

const fs = require('fs');
const path = require('path');
const Sequelize = require('sequelize');
// const oracledbSian = require('oracledbSian');
// oracledbSian.initOracleClient({ libDir: '' });
const process = require('process');
const basename = path.basename(__filename);
const env = process.env.NODE_ENV || 'sian';
const config = require(__dirname + '/../../config/config.json')[env];
const dbSian = {};

let sequelize;
if (config.use_env_variable) {
    sequelize = new Sequelize(process.env[config.use_env_variable], config);
} else {
    sequelize = new Sequelize(config.database, config.username, config.password, config);
}

fs
    .readdirSync(__dirname)
    .filter(file => {
        return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
    })
    .forEach(file => {
        const model = require(path.join(__dirname, file));
        // const model = require(path.join(__dirname, file))(sequelize, Sequelize.DataTypes);
        dbSian[model.name] = model;
    });

Object.keys(dbSian).forEach(modelName => {
    if (dbSian[modelName].associate) {
        dbSian[modelName].associate(dbSian);
    }
});

dbSian.sequelize = sequelize;
dbSian.Sequelize = Sequelize;

module.exports = dbSian;
