const Sequelize = require("sequelize-oracle");

module.exports = (sequelize, DataTypes) => {
    const user = sequelize.define("USERNODE", {
        id: {
            field: 'ID',
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        username: {
            field: 'USERNAME',
            type: Sequelize.STRING,
            required: true,
            allowNull: null,
            len: [6, 20],
        },
        clave: {
            field: 'CLAVE',
            type: Sequelize.STRING,
            required: true,
            allowNull: null,
            len: [6, 20]
        }
    }, {
        tableName: 'USERNODE',
        timestamps: false,
        underscored: true,
        paranoid: true
    });
    return user;
}