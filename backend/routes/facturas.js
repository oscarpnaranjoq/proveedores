const { json } = require('body-parser');
const express = require('express');
router = express.Router();
const { _findFacturasByParams } = require('../controllers/facturasController'),
    auth = require('../middleware/auth');

router.post('/listFacturasProveedor', auth, async (req, res, next) => {
    try {
        const facturas = await _findFacturasByParams(req.body);
        return res.status(200).json(facturas);
    } catch (e) {
        return res.status(500).json(e.message);
    }
});

module.exports = router;