const { json } = require('body-parser');
const express = require('express');
router = express.Router();
const { _findOrdenesById, _findDetailOrdenesById } = require('../controllers/ordenesController'),
    auth = require('../middleware/auth');

router.post('/ordenesCompra', auth, async (req, res, next) => {
    try {
        const ordenes = await _findOrdenesById(req.body);
        return res.status(200).json(ordenes);
    } catch (e) {
        return res.status(500).json(e.message);
    }
});
router.post('/listDetalleOrdenesCompra', auth, async (req, res, next) => {
    try {
        console.log(req.body)
        const detalle = await _findDetailOrdenesById(req.body);
        return res.status(200).json(detalle);
    } catch (e) {
        return res.status(500).json(e.message);
    }
});

module.exports = router;