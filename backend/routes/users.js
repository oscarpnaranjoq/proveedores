const { json } = require('body-parser');
const express = require('express');
router = express.Router();
const { _findAll, _create, _updateCreateInfo } = require('../controllers/userController'),
    auth = require('../middleware/auth');

router.get('/', async (req, res, next) => {
    try {
        const users = await _findAll();
        return res.status(200).json(users);
    } catch (e) {
        return res.status(500).json(e.message);
    }
});
router.post('/createUser', auth, async (req, res, next) => {
    try {
        const users = await _create(req.body);
        return res.status(200).json(users);
    } catch (e) {
        return res.status(500).json(e.message);
    }
});

router.post('/saveInfoProfile', auth, async (req, res, next) => {
    try {
        const info = await _updateCreateInfo(req.body);
        return res.status(200).json({
            status: "success",
            message: info
        });
    } catch (e) {
        return res.status(500).json({ error: e.message });
    }
})

module.exports = router;