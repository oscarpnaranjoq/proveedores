const express = require('express');
router = express.Router();
const { _create, _findByUserName, _findByUsuario, _resetPassword } = require('../controllers/userController');
const passport = require('passport');
const jwt = require('jsonwebtoken');
const { generatePWD } = require('../helpers/generatePWD')

router.post('/sign-in', async (req, res, next) => {
    passport.authenticate('local', { session: false }, function (err, user, info) {
        if (err) return res.status(500).json(err);
        if (!user) return res.status(400).json(info);
        const access_token = jwt.sign(user, process.env.SECRET_KEY, { expiresIn: '1h' });
        return res.status(200).json(
            infoUser(user, access_token)
        );
    })(req, res, next);
});
router.post('/access-token', async (req, res, next) => {
    let jwtSecretKey = process.env.SECRET_KEY;
    try {
        const token = req.body.access_token;
        const verified = jwt.verify(token, jwtSecretKey);
        if (verified) {
            const user = await _findByUsuario(verified);
            const newData = {
                usuario: user.USU_RUC,
                username: user.USU_RUC,
                pwd: user.USU_PSW,
                emails: user.EMAILS,
                codUsuario: user.COD_PROVEEDOR,
                id: user.USU_RUC,
                menu: user.MENU,
                routes: user.ROUTES,
            }
            return res.status(200).json(
                infoUser(newData, token)
            );
        } else {
            // Access Denied
            return res.status(401).send(error);
        }
    } catch (error) {
        // Access Denied
        return res.status(401).send(error);
    }
});
router.post('/sign-up', async (req, res) => {
    try {
        const data = req.body;
        const findUser = await _findByUsuario(data);
        if (findUser) {
            return res.status(400).json(
                {
                    status: 'error',
                    message: 'Usuario ya existe'
                });
        }
        data.password = generatePWD();
        const user = await _create(data);
        return res.status(201).json({
            status: 'success',
            message: user
        });
    } catch (e) {
        return res.status(500).json(e.message);
    }
});
router.post('/forgot-password', async (req, res) => {
    try {
        const data = req.body;
        const findUser = await _findByUsuario(data);
        if (!findUser) {
            return res.status(400).json(
                {
                    status: 'error',
                    message: 'Usuario no existe'
                });
        }
        data.emails = findUser.EMAILS;
        data.userId = data.username;
        data.password = generatePWD();
        const reset = await _resetPassword(data);
        return res.status(201).json({
            status: 'success',
            message: reset
        });
    } catch (e) {
        return res.status(500).json(e.message);
    }
});
const infoUser = (user, access_token) => {
    console.log("------------infoooo")
    console.log(user)
    return {
        user: {
            uuid: 'XgbuVEXBU5gtSKdbQRP1Zbbby1i1',
            role: 'personal',
            from: 'example@example.com',
            // exampleMenu: user.menu,
            // exampleRoutes: user.routes,
            data: {
                userId: user.id,
                displayName: user.usuario,
                email: '',
                emails: user.emails === null ? [] : user.emails.split(","),
                pwd: user.pwd,
                codUsuario: user.codUsuario === null ? '' : user.codUsuario,
                photoURL: 'assets/images/avatars/usuario.jpg',
                setting: {
                    layout: {},
                    theme: {}
                },
                shortcuts: ["apps.calendar", "apps.mailbox", "apps.contacts"],
                menu: user.menu,
                routes: user.routes
                // menu: [{
                //     id: 'estadocuenta',
                //     title: 'Estado de Cuenta',
                //     type: 'group',
                //     icon: 'verified_user',
                //     children: [
                //         {
                //             id: 'facturas',
                //             title: 'Facturas',
                //             translate: 'Facturas',
                //             type: 'item',
                //             icon: 'heroicons-outline:document-text',
                //             url: 'facturas',
                //             app: 'facturas'
                //         },
                //     ],
                // }, {
                //     id: 'auth',
                //     title: 'Auth',
                //     type: 'group',
                //     icon: 'verified_user',
                //     children: [
                //         {
                //             id: 'sign-out',
                //             title: 'Cerrar Sesión',
                //             type: 'item',
                //             url: 'sign-out',
                //             icon: 'exit_to_app',
                //         },
                //     ],
                // }],
                // routes: [{
                //     path: "facturas"
                // }, {
                //     path: "sign-out"
                // }, {
                //     path: "sign-in"
                // }, {
                //     path: "/"
                // }, {
                //     path: "loading"
                // }, {
                //     path: "404"
                // }, {
                //     path: "*"
                // }]
            }
        },
        access_token,
        expiresIn: 3600,
    }
}

module.exports = router;