import FuseUtils from '@fuse/utils';
import FuseLoading from '@fuse/core/FuseLoading';
import { Navigate } from 'react-router-dom';
import settingsConfig from 'app/configs/settingsConfig';
import SignInConfig from '../main/sign-in/SignInConfig';
import SignUpConfig from '../main/sign-up/SignUpConfig';
import SignOutConfig from '../main/sign-out/SignOutConfig';
import forgotPasswordConfig from '../main/forgot-password/forgotPasswordConfig';
import Error404Page from '../main/404/Error404Page';
import ExampleConfig from '../main/example/ExampleConfig';
import EmpleadoConfig from '../main/empleado/EmpleadoConfig';
import ComprasConfig from '../main/compras/ComprasConfig';
import FacturasConfig from '../main/facturas/FacturasConfig';
import TanquerosConfig from '../main/tanqueros/TanquerosConfig';

const routeConfigs = [TanquerosConfig, FacturasConfig, ComprasConfig, EmpleadoConfig, SignOutConfig, SignInConfig, SignUpConfig, forgotPasswordConfig];
const routes = [
  ...FuseUtils.generateRoutesFromConfigs(routeConfigs, settingsConfig.defaultAuth),
  {
    path: '/',
    element: <Navigate to="/facturas" />,
    auth: settingsConfig.defaultAuth,
  },
  {
    path: 'loading',
    element: <FuseLoading />,
  },
  {
    path: '404',
    element: <Error404Page />,
  },
  {
    path: '*',
    element: <Navigate to="404" />,
  },
];
// let result = menu
//   .flatMap(({ children, ...menu }) => [menu, ...children])
//   .filter((item) => item.url)
//   // .map((item) => item.id);

// console.log(result)
// console.log("******************************")
// console.log(routes)
// var result = routes.filter(function (o1) {
//   return menu.find(function (o2) {
//     console.log(o2.children)
//     // return o1.path === o2.url; // return the ones with equal id
//   });
// });
// console.log("----------------------------")
// console.log(result)
export default routes;
