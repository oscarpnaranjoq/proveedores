import { styled } from '@mui/material/styles';
import FusePageCarded from '@fuse/core/FusePageCarded';
import { useState, Fragment, useEffect } from 'react';
import Box from '@mui/material/Box';
import Stepper from '@mui/material/Stepper';
import Step from '@mui/material/Step';
import StepButton from '@mui/material/StepButton';
import Typography from '@mui/material/Typography';
import MUIDataTable from "mui-datatables";
import spanishMUIDatatable from '../../shared-components/spanishMUIDatatable';
import { createTheme, ThemeProvider } from '@mui/material/styles';
const textLabels = spanishMUIDatatable;
const Root = styled(FusePageCarded)(({ theme }) => ({
    '& .FusePageSimple-header': {
        backgroundColor: theme.palette.background.paper,
        borderBottomWidth: 1,
        borderStyle: 'solid',
        borderColor: theme.palette.divider,
    },
    '& .FusePageSimple-toolbar': {},
    '& .FusePageSimple-content': {},
    '& .FusePageSimple-sidebarHeader': {},
    '& .FusePageSimple-sidebarContent': {},
}));
const getMuiTheme = () => createTheme({
    components: {
        MUIDataTableBodyCell: {
            styleOverrides: {
                root: {
                    backgroundColor: "#FF0000"
                }
            }
        }
    }
})

const steps = ['En Proceso', 'Cotizado', 'Comprado', 'Entregado'];
const columns = [
    {
        name: "ITEM",
        label: "ITEM",
        options: {
            filter: true,
            sort: true,
        }
    },
    {
        name: "NARICULO",
        label: "ARTICULO",
        options: {
            filter: true,
            sort: true,
        }
    },
    {
        name: "CANTIDAD",
        label: "CANTIDAD",
        options: {
            filter: true,
            sort: true,
        }
    },
    {
        name: "UM",
        label: "TIPO",
        options: {
            filter: true,
            sort: true,
        }
    },
];

const options = {
    filter: false,
    selectableRows: 'none',
    print: false,
    responsive: 'vertical',
    print: false,
    download: false,
    textLabels,
};

function ComprasStepper(props) {
    let proceso = props.detalle.proceso;
    let cotizado = props.detalle.cotizado;
    let comprado = props.detalle.comprado;
    let entregado = props.detalle.entregado;
    const activo = () => {
        let activo;
        if (proceso.length > cotizado.length && proceso.length > comprado.length && proceso.length > entregado.length) {
            activo = 0;
        }
        if (cotizado.length > proceso.length && cotizado.length > comprado.length && cotizado.length > entregado.length) {
            activo = 1;
        }
        if (comprado.length > proceso.length && comprado.length > cotizado.length && comprado.length > entregado.length) {
            activo = 2;
        }
        if (entregado.length > proceso.length && entregado.length > cotizado.length && entregado.length > comprado.length) {
            activo = 3;
        }
        return activo === undefined ? 0 : activo;

    }
    const [activos, setActivos] = useState({});
    const [activeStep, setActiveStep] = useState(activo);
    const [completed, setCompleted] = useState({});

    useEffect(() => {
        //ACTIVA LOS STEPS QUE CONTENGAN DATOS Y CAMBIA EL ITEM A CHECK
        let newCompleted = {};
        if (proceso.length > 0) {
            newCompleted[0] = true;
        }
        if (cotizado.length > 0) {
            newCompleted[1] = true;
        }
        if (comprado.length > 0) {
            newCompleted[2] = true;
        }
        if (entregado.length > 0) {
            newCompleted[3] = true;
        }
        setCompleted(newCompleted)
        activo()
    });


    const handleStep = (step) => () => {
        // console.log(completed)
        // const newCompleted = completed;
        // newCompleted[activeStep] = true;
        // setCompleted(newCompleted);
        setActiveStep(step);
        // handleNext();
    };

    const getStepContent = (step) => {
        switch (step) {
            case 1:
                return (
                    <MUIDataTable
                        data={proceso}
                        columns={columns}
                        options={options} />
                );
            case 2:
                return (
                    <MUIDataTable
                        data={cotizado}
                        columns={columns}
                        options={options} />
                );
            case 3:
                return (
                    <MUIDataTable
                        data={comprado}
                        columns={columns}
                        options={options} />
                );
            case 4:
                return (
                    <MUIDataTable
                        data={entregado}
                        columns={columns}
                        options={options} />
                );
            default:
                return 'Unknown step';
        }
    }
    return (
        <div className="flex h-1/2 justify-left items-left mr-auto ml-auto flex-wrap container">
            <Box sx={{ width: '100%' }}>
                <Stepper nonLinear activeStep={activeStep}>
                    {steps.map((label, index) => (
                        <Step key={label} completed={completed[index]}>
                            <StepButton color="inherit" onClick={handleStep(index)}>
                                {label}
                            </StepButton>
                        </Step>
                    ))}
                </Stepper>
                <div>
                    <Fragment>
                        <Typography component={'span'} sx={{ mt: 2, mb: 1, py: 1 }}>
                            {getStepContent(activeStep + 1)}
                        </Typography>
                    </Fragment>
                </div>
            </Box>
        </div>
    );
}

export default ComprasStepper;
