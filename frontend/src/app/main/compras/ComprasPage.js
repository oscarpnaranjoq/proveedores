import { styled } from '@mui/material/styles';
import { useTranslation } from 'react-i18next';
import FusePageCarded from '@fuse/core/FusePageCarded';
import * as React from 'react';
import Box from '@mui/material/Box';
import Stepper from '@mui/material/Stepper';
import Step from '@mui/material/Step';
import StepButton from '@mui/material/StepButton';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import ComprasTable from './ComprasTable'
import comprasSearch from './ComprasSearch';
import { useDispatch, useSelector } from 'react-redux';
import { selectCompras } from './store/comprasSlice';

const Root = styled(FusePageCarded)(({ theme }) => ({
    '& .FusePageSimple-header': {
        backgroundColor: theme.palette.background.paper,
        borderBottomWidth: 1,
        borderStyle: 'solid',
        borderColor: theme.palette.divider,
    },
    '& .FusePageSimple-toolbar': {},
    '& .FusePageSimple-content': {},
    '& .FusePageSimple-sidebarHeader': {},
    '& .FusePageSimple-sidebarContent': {},
}));

function ComprasPage(props) {
    const { t } = useTranslation('ComprasPage');

    return (
        <Root
            header={
                <div className="p-24">
                    <h4>{t('TRACKING COMPRAS')}</h4>
                </div>
            }
            content={
                <div className="p-24">
                    {comprasSearch()}
                </div>
            }
            scroll="page"
        />
    );
}

export default ComprasPage;
