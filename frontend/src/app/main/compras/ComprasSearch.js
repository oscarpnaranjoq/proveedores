import { Controller, useForm } from 'react-hook-form';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import InputLabel from '@mui/material/InputLabel';
import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';
import _ from '@lodash';
import FormControl from '@mui/material/FormControl';
import FormHelperText from '@mui/material/FormHelperText';
import { DesktopDatePicker } from '@mui/x-date-pickers/DesktopDatePicker';
import { useDispatch, useSelector } from 'react-redux';
import moment from 'moment';
// import { setCompras } from './store/comprasSlice';
import { selectUser } from 'app/store/userSlice';
import FuseSvgIcon from '@fuse/core/FuseSvgIcon';
import { useEffect, useMemo, useState } from 'react';
import FuseLoading from '@fuse/core/FuseLoading';
import ComprasBody from './ComprasBody';
import NoFoundRecords from 'app/shared-components/NoFoundRecords'; '../../shared-components/NoFoundRecords';
import axios from 'axios';

const defaultValues = {
    fechaInicio: '',
    fechaFinal: '',
    tipoDocumento: ''
};

/**
 * Form Validation Schema
 */
const schema = yup.object().shape({
    fechaInicio: yup.string().nullable().required('Seleccione una fecha.'),
    fechaFinal: yup.string().nullable().required('Seleccione una fecha.'),
    tipoDocumento: yup.string().required('Seleccione una opcion'),
});

function comprasSearch() {
    const [compras, setCompras] = useState([]);
    const [loading, setLoading] = useState(null);
    const user = useSelector(selectUser);
    const dispatch = useDispatch();
    const { handleSubmit, register, reset, control, watch, formState } = useForm({
        defaultValues,
        mode: 'all',
        resolver: yupResolver(schema),
    });
    const findComprasWithParameters = (data) => {
        // setCompras([{"SOLICITUD":22001333,"TIPO":"OR","PHKCOO":"00106","FECHA":"01-11-2022","DESTINO_ENVIO":"BODEGA REPUESTOS                        ","SOLICITANTE":"POZO BAHAMONDE BORIS PATRICIO           ","OBSERVACIONES":"DISCO DURO 12TB                 Originador : EPILAGUANO"},{"SOLICITUD":22001635,"TIPO":"OR","PHKCOO":"00105","FECHA":"01-11-2022","DESTINO_ENVIO":"BODEGA REPUESTOS                        ","SOLICITANTE":"POZO BAHAMONDE BORIS PATRICIO           ","OBSERVACIONES":"DISCO DURO 12 TB                Originador : EPILAGUANO"}])
        setLoading(true);
        return new Promise((resolve, reject) => {
            axios.post('/api/tracking/ordenesCompra', data)
                .then((response) => {
                    setCompras(response.data);
                    setLoading(false);
                })
                .catch((error) => {
                    setLoading(false);
                    setCompras([]);
                    console.log(error)
                });
        });
    };
    if (loading) {
        return (
            <div className="flex items-center justify-center h-full">
                <FuseLoading />
            </div>
        );
    }
    const { isValid, dirtyFields, errors, touchedFields } = formState;
    const data = watch();
    return (
        <div className="container mx-auto">
            <form className="w-full max-w-lg" onSubmit={handleSubmit((_data) => {
                _data.identificacion = user.data.userId
                findComprasWithParameters(_data)
                // dispatch(findCompras(_data))
            })}>
                <div className="flex flex-row gap-4 mb-4">
                    <div className="basis-1/4">
                        <div className="w-full mt-20">
                            <Controller
                                name="fechaInicio"
                                control={control}
                                render={({ field: { onChange, value, onBlur } }) => (
                                    <DesktopDatePicker
                                        label="Fecha Inicio"
                                        inputFormat="yyyy-MM-dd"
                                        value={moment(value).add(1, 'Days').format("YYYY-MM-DD")}
                                        onChange={(e) =>
                                            onChange(moment(e).format("YYYY-MM-DD"))
                                        }
                                        renderInput={(_props) => (
                                            <TextField
                                                className="w-full"
                                                {..._props}
                                                size="small"
                                                onBlur={onBlur}
                                                error={!!errors.fechaInicio}
                                                helperText={errors?.fechaInicio?.message}
                                            />
                                        )}
                                    />
                                )}
                            />
                        </div>
                    </div>
                    <div className="basis-1/4">
                        <div className="w-full mt-20">
                            <Controller
                                name="fechaFinal"
                                control={control}
                                render={({ field: { onChange, value, onBlur } }) => (
                                    <DesktopDatePicker
                                        label="Fecha Final"
                                        inputFormat="yyyy-MM-dd"
                                        value={moment(value).add(1, 'Days').format("YYYY-MM-DD")}
                                        onChange={(e) =>
                                            onChange(moment(e).format("YYYY-MM-DD"))
                                        }
                                        renderInput={(_props) => (
                                            <TextField
                                                className="w-full"
                                                {..._props}
                                                onBlur={onBlur}
                                                size="small"
                                                error={!!errors.fechaFinal}
                                                helperText={errors?.fechaFinal?.message}
                                            />
                                        )}
                                    />
                                )}
                            />
                        </div>
                    </div>
                    <div className="basis-1/4">
                        <div className="w-full mt-20">
                            <Controller
                                name="tipoDocumento"
                                control={control}
                                render={({ field }) => (
                                    <FormControl error={!!errors.tipoDocumento} required fullWidth size="small">
                                        <InputLabel id="demo-simple-select-label">Tipo Documento</InputLabel>
                                        <Select
                                            labelId="demo-select-small"
                                            label="Tipo Documento"
                                            {...field}
                                        >
                                            <MenuItem value="">
                                                <em>Seleccione</em>
                                            </MenuItem>
                                            <MenuItem value='OR'>OR</MenuItem>
                                        </Select>
                                        <FormHelperText>{errors?.tipoDocumento?.message}</FormHelperText>
                                    </FormControl>
                                )}
                            />
                        </div>
                    </div>
                    <div className="basis-1/4">
                        <div className="w-full mt-20">
                            <Button
                                variant="contained"
                                type="submit"
                                color="secondary"
                                startIcon={<FuseSvgIcon>heroicons-outline:search</FuseSvgIcon>}
                                disabled={_.isEmpty(dirtyFields) || !isValid}
                            >
                                Buscar
                            </Button>
                        </div>
                    </div>
                </div>
            </form>
            {compras.length === 0 ? (
                <NoFoundRecords />) : <ComprasBody compras={compras} />}
        </div>
    );
}

export default comprasSearch;