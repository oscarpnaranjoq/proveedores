import { styled } from '@mui/material/styles';
import FusePageCarded from '@fuse/core/FusePageCarded';
import { useState, Fragment } from 'react';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import List from '@mui/material/List';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import InboxIcon from '@mui/icons-material/Inbox';
import LocalShippingIcon from '@mui/icons-material/LocalShipping';
import ApartmentIcon from '@mui/icons-material/Apartment';
import ListSubheader from "@mui/material/ListSubheader";
import ComprasStepper from './ComprasStepper'
import { listDetalle } from '../../services/tracking/trackingServices';
import trackingServiceConfig from '../../services/tracking/trackingServiceConfig';
import axios from 'axios';
import FuseLoading from '@fuse/core/FuseLoading';

const Root = styled(FusePageCarded)(({ theme }) => ({
    '& .FusePageSimple-header': {
        backgroundColor: theme.palette.background.paper,
        borderBottomWidth: 1,
        borderStyle: 'solid',
        borderColor: theme.palette.divider,
    },
    '& .FusePageSimple-toolbar': {},
    '& .FusePageSimple-content': {},
    '& .FusePageSimple-sidebarHeader': {},
    '& .FusePageSimple-sidebarContent': {},
}));



function ComprasBody(props) {
    console.log(props)
    const [selectedIndex, setSelectedIndex] = useState(1);
    const [selectedItem, setSelectedItem] = useState(0);
    const [detalle, setDetalle] = useState([]);
    const [proceso, setProceso] = useState([]);
    const [cotizado, setCotizado] = useState([]);
    const [comprado, setComprado] = useState([]);
    const [entregado, setEntregado] = useState([]);
    const [loading, setLoading] = useState(null);

    const handleListItemClick = (event, index, compania, tipo) => {
        let data = {
            compania: compania,
            idCabecera: String(index),
            tipoDocumento: tipo
        }
        console.log(data)
        setSelectedIndex(index);
        setSelectedItem(1);
        findDetalleWithParameters(data)

    };
    const findDetalleWithParameters = (data) => {
        setLoading(true);
        return new Promise((resolve, reject) => {
            axios.post(trackingServiceConfig.listDetalle, data)
                .then((response) => {
                    console.log(response.data)
                    let d = response.data;
                    const p = d.filter((e) => {
                        if (e.NUMCOT === "" && e.NUMORDEN === "" && e.CANTREC === 0) {
                            return e;
                        }
                    })
                    const cot = d.filter((e) => {
                        if (e.NUMCOT !== "" && e.NUMORDEN === "" && e.CANTREC === 0) {
                            return e;
                        }
                    })
                    const com = d.filter((e) => {
                        if (e.NUMCOT !== "" && e.NUMORDEN !== "" && e.CANTREC === 0) {
                            return e;
                        }
                    })
                    const e = d.filter((e) => {
                        if (e.NUMCOT !== "" && e.NUMORDEN !== "" && e.CANTREC !== 0) {
                            return e;
                        }
                    })
                    setDetalle({ proceso: p, cotizado: cot, comprado: com, entregado: e })
                    // setDetalle(response.data);
                    setLoading(false);
                })
                .catch((error) => {
                    setLoading(false);
                    setDetalle([]);
                    console.log(error)
                });
        });
    };
    return (
        <div className="p-24">
            <div className="flex h-1/2 justify-left items-left mr-auto ml-auto flex-wrap container">
                <div className="pr-4 pl-4 w-full h-full flex item-left justify-left md:w-1/4 md:mb-0">
                    <Box sx={{ width: '100%', bgcolor: 'background.paper', overflowX: "scroll", overflowY: "scroll", height: "100vh", display: "flex", flexDirection: "column" }}>
                        <List component="nav" subheader={
                            <ListSubheader component="div">PEDIDOS</ListSubheader>
                        }>
                            {props.compras.map((_item) => {
                                return (
                                    <div key={_item.SOLICITUD}>
                                        <ListItemButton
                                            selected={selectedIndex === _item.SOLICITUD}
                                            onClick={(event) => handleListItemClick(event, _item.SOLICITUD, _item.COMPANIA, _item.TIPO)}
                                        >
                                            <ListItemText
                                                primary={
                                                    <Fragment>
                                                        <Typography
                                                            sx={{ display: 'inline' }}
                                                            component="span"
                                                            variant="body2"
                                                            color="text.primary"
                                                        >
                                                            <ApartmentIcon /> {_item.NCOMPANIA}
                                                        </Typography>
                                                    </Fragment>}
                                                secondary={
                                                    <Fragment>
                                                        <Typography
                                                            sx={{ display: 'inline' }}
                                                            component="span"
                                                            variant="body2"
                                                            color="text.primary"
                                                            fontSize={10}
                                                        >
                                                            <LocalShippingIcon fontSize="small" /> {_item.DESTINO_ENVIO}
                                                        </Typography>
                                                        <br />
                                                        <Typography
                                                            component="span"
                                                            fontSize={10}
                                                        >{_item.FECHA} - {_item.OBSERVACIONES}</Typography>
                                                    </Fragment>} />
                                        </ListItemButton>
                                    </div>
                                )
                            })}
                        </List>
                    </Box>
                </div>
                <div className="pr-4 pl-4 w-full h-full md:w-2/3 md:mb-0">
                    {(loading) ? (
                        <div className="flex items-center justify-center">
                            <FuseLoading />
                        </div>) : (selectedItem === 1 ? <ComprasStepper detalle={detalle} /> : "")
                    }
                </div>
            </div>
        </div>
    );
}

export default ComprasBody;
