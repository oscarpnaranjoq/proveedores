import { styled } from '@mui/material/styles';
import { useTranslation } from 'react-i18next';
import FusePageCarded from '@fuse/core/FusePageCarded';
import * as React from 'react';
import FacturasSearch from './FacturasSearch';

const Root = styled(FusePageCarded)(({ theme }) => ({
    '& .FusePageSimple-header': {
        backgroundColor: theme.palette.background.paper,
        borderBottomWidth: 1,
        borderStyle: 'solid',
        borderColor: theme.palette.divider,
    },
    '& .FusePageSimple-toolbar': {},
    '& .FusePageSimple-content': {},
    '& .FusePageSimple-sidebarHeader': {},
    '& .FusePageSimple-sidebarContent': {},
}));

function FacturasPage(props) {
    const { t } = useTranslation('FacturasPage');

    return (
        <Root
            header={
                <div className="p-24">
                    <h4>{t('FACTURAS')}</h4>
                </div>
            }
            content={
                <div className="p-24">
                    {FacturasSearch()}
                </div>
            }
            scroll="page"
        />
    );
}

export default FacturasPage;
