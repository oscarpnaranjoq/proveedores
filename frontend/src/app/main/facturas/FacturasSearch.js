import { Controller, useForm } from 'react-hook-form';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import InputLabel from '@mui/material/InputLabel';
import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';
import _ from '@lodash';
import FormControl from '@mui/material/FormControl';
import FormHelperText from '@mui/material/FormHelperText';
import { DesktopDatePicker } from '@mui/x-date-pickers/DesktopDatePicker';
import { useDispatch, useSelector } from 'react-redux';
import moment from 'moment';
// import { setCompras } from './store/comprasSlice';
import { selectUser } from 'app/store/userSlice';
import FuseSvgIcon from '@fuse/core/FuseSvgIcon';
import { useEffect, useMemo, useState } from 'react';
import FuseLoading from '@fuse/core/FuseLoading';
import NoFoundRecords from 'app/shared-components/NoFoundRecords';
import axios from 'axios';
import FacturasTable from './FacturasTable';

const defaultValues = {
    fechaInicio: '',
    fechaFinal: '',
    sEmpresa: '',
    numFactura: '',
};

/**
 * Form Validation Schema
 */
const schema = yup.object().shape({
    fechaInicio: yup.string(),
    fechaFinal: yup.string(),
    sEmpresa: yup.string(),
    numFactura: yup.string(),
});

function FacturasSearch() {
    const [facturas, setFacturas] = useState([]);
    const [empresas, setEmpresas] = useState([]);
    const [loading, setLoading] = useState(null);
    const user = useSelector(selectUser);
    const { handleSubmit, register, reset, control, watch, formState } = useForm({
        defaultValues,
        mode: 'all',
        resolver: yupResolver(schema),
    });
    const fac = {
        identificacion: user.data.userId,
        fechaInicio: "",
        fechaFin: "",
        sEmpresa: "",
        numFactura: ""
    }

    useEffect(() => {
        findFacturas(fac)
    }, []);

    const findFacturas = (data) => {
        setLoading(true);
        axios.post("/api/facturas/listFacturasProveedor", data)
            .then(({ data }) => {
                setFacturas(data.facturas);
                setEmpresas(data.empresas);
                setLoading(false);
            })
            .catch((error) => {
                setLoading(false);
                setFacturas([]);
                setEmpresas([]);
            });
    };
    const findFacturasWithParameters = (data) => {
        findFacturas(data)
    };
    if (loading) {
        return (
            <div className="flex items-center justify-center h-full">
                <FuseLoading />
            </div>
        );
    }
    const { isValid, dirtyFields, errors, touchedFields } = formState;
    const data = watch();
    return (
        <div className="container mx-auto">
            <form className="w-full max-w-lg" onSubmit={handleSubmit((_data) => {
                _data.identificacion = user.data.userId
                findFacturasWithParameters(_data)
                // dispatch(findCompras(_data))
            })}>
                <div className="flex flex-row gap-4 mb-4">
                    <div className="basis-1/4">
                        <div className="w-full mt-20">
                            <Controller
                                name="fechaInicio"
                                control={control}
                                render={({ field: { onChange, value, onBlur } }) => (
                                    <DesktopDatePicker
                                        label="Fecha Inicio"
                                        inputFormat="yyyy-MM-dd"
                                        value={moment(value).add(1, 'Days').format("YYYY-MM-DD")}
                                        onChange={(e) => onChange(moment(e).format("YYYY-MM-DD"))}
                                        renderInput={(_props) => (
                                            <TextField
                                                className="w-full"
                                                {..._props}
                                                size="small"
                                                onBlur={onBlur}
                                                error={!!errors.fechaInicio}
                                                helperText={errors?.fechaInicio?.message}
                                            />
                                        )}
                                    />
                                )}
                            />
                        </div>
                    </div>
                    <div className="basis-1/4">
                        <div className="w-full mt-20">
                            <Controller
                                name="fechaFinal"
                                control={control}
                                render={({ field: { onChange, value, onBlur } }) => (
                                    <DesktopDatePicker
                                        label="Fecha Final"
                                        inputFormat="yyyy-MM-dd"
                                        value={moment(value).add(1, 'Days').format("YYYY-MM-DD")}
                                        onChange={(e) => onChange(moment(e).format("YYYY-MM-DD"))}
                                        renderInput={(_props) => (
                                            <TextField
                                                className="w-full"
                                                {..._props}
                                                onBlur={onBlur}
                                                size="small"
                                                error={!!errors.fechaFinal}
                                                helperText={errors?.fechaFinal?.message}
                                            />
                                        )}
                                    />
                                )}
                            />
                        </div>
                    </div>
                    <div className="basis-1/4">
                        <div className="w-full mt-20">
                            <Controller
                                name="sEmpresa"
                                control={control}
                                render={({ field }) => (
                                    <FormControl error={!!errors.sEmpresa} fullWidth size="small">
                                        <InputLabel id="demo-simple-select-label">Empresa</InputLabel>
                                        <Select
                                            labelId="demo-select-small"
                                            label="Empresa"
                                            {...field}
                                        >
                                            <MenuItem value=""><em>Seleccione</em></MenuItem>
                                            {empresas.map(x =>
                                                <MenuItem key={x.COD_EMPRESA} value={x.COD_EMPRESA}>{x.NOMBRE_EMPRESA}</MenuItem>
                                            )}
                                        </Select>
                                        <FormHelperText>{errors?.sEmpresa?.message}</FormHelperText>
                                    </FormControl>
                                )}
                            />
                        </div>
                    </div>
                    <div className="basis-1/4">
                        <div className="w-full mt-20">
                            <Controller
                                render={({ field }) => (
                                    <TextField
                                        {...field}
                                        label="Factura"
                                        variant="outlined"
                                        error={!!errors.numFactura}
                                        helperText={errors?.numFactura?.message}
                                        size="small"
                                        fullWidth
                                    />
                                )}
                                name="numFactura"
                                control={control}
                            />
                        </div>
                    </div>
                    <div className="basis-1/4">
                        <div className="w-full mt-20">
                            <Button
                                variant="contained"
                                type="submit"
                                color="secondary"
                                startIcon={<FuseSvgIcon>heroicons-outline:search</FuseSvgIcon>}
                            >
                                Buscar
                            </Button>
                        </div>
                    </div>
                </div>
            </form>
            {facturas.length === 0 ? (
                <NoFoundRecords />) : <FacturasTable facturas={facturas} />}
        </div>
    );
}

export default FacturasSearch;