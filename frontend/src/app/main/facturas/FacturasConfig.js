import FacturasPage from './FacturasPage';

const FacturasConfig = {
    settings: {
        layout: {
            config: {},
        },
    },
    routes: [
        {
            path: 'facturas',
            element: <FacturasPage />,
        },
    ],
};

export default FacturasConfig;