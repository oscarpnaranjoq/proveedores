import Box from '@mui/material/Box';
import MUIDataTable from "mui-datatables";
import Chip from '@mui/material/Chip';
import spanishMUIDatatable from '../../shared-components/spanishMUIDatatable';
import moment from 'moment';

const textLabels = spanishMUIDatatable;
const columns = [
    {
        name: "NOMBRE_EMPRESA",
        label: "EMPRESA",
        options: {
            filter: true,
            sort: true,
        }
    },
    {
        name: "NUM_FACTURA",
        label: "# FACTURA",
        options: {
            filter: true,
            sort: true,
        }
    },
    {
        name: "FECHA_FACTURA",
        label: "FECHA FACTURA",
        options: {
            filter: true,
            sort: true,
            customBodyRender: value => moment(value).format("DD-MM-YYYY")
        }
    },
    {
        name: "IMPORTE_BRUTO",
        label: "IMP. BRUTO",
        options: {
            filter: true,
            sort: true,
            customBodyRender: value => (<Chip label={Number(value).toFixed(2)} color="success" size="small" />)
        }
    },
    {
        name: "IMPORTE_PAGO",
        label: "IMP. PAGO",
        options: {
            filter: true,
            sort: true,
            customBodyRender: value => (<Chip label={Number(value).toFixed(2)} color="success" size="small" />)
        }
    }, {
        name: "FECHA_PAGO",
        label: "FECHA PAGO",
        options: {
            filter: true,
            sort: true,
            customBodyRender: value => moment(value).format("DD-MM-YYYY")
        }
    }, {
        name: "CUENTA_PAGO",
        label: "CUENTA PAGO",
        options: {
            filter: true,
            sort: true,
        }
    }
];

const options = {
    filter: true,
    selectableRows: 'none',
    print: true,
    responsive: 'vertical',
    print: false,
    download: false,
    textLabels,
};

function FacturasTable(props) {
    const { facturas } = props;
    return (
        <div className="flex h-1/2 justify-left items-left mr-auto ml-auto flex-wrap container">
            <Box sx={{ width: '100%' }}>
                <MUIDataTable
                    title={"DETALLE FACTURAS PAGADAS"}
                    data={facturas}
                    columns={columns}
                    options={options} />
            </Box>
        </div >
    );
}

export default FacturasTable;
