import ModernForgotPasswordPage from './ModernForgotPasswordPage';

const forgotPasswordConfig = {
  settings: {
    layout: {
      config: {
        navbar: {
          display: false,
        },
        toolbar: {
          display: false,
        },
        footer: {
          display: false,
        },
        leftSidePanel: {
          display: false,
        },
        rightSidePanel: {
          display: false,
        },
      },
    },
  },
  auth: null,
  routes: [
    {
      path: 'forgot-password',
      element: <ModernForgotPasswordPage />,
    },
  ],
};
export default forgotPasswordConfig;

