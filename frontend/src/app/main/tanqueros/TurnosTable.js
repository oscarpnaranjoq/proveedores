import Box from '@mui/material/Box';
import MUIDataTable from "mui-datatables";
import Chip from '@mui/material/Chip';
import spanishMUIDatatable from '../../shared-components/spanishMUIDatatable';
import moment from 'moment';
import IconButton from '@mui/material/IconButton';
import CheckIcon from '@mui/icons-material/Check';
import DeleteIcon from '@mui/icons-material/Delete';
import ConfirmDialog from './ConfirmDialog';
import { useState } from 'react';

function TurnosTable(props) {
    const { facturas } = props;
    const [open, setOpen] = useState(false);
    const textLabels = spanishMUIDatatable;
    const data = [
        { fecha: "15/03/2023", tarea: "P15", producto: "HIBRIDO", estado: "CREADO", placa: "PBX0123", chofer: "JUAN PEREZ", km: "30KM", tarifa: "15%", correos: "a@a.com,b@b.com" },
        { fecha: "15/03/2023", tarea: "P16", producto: "HIBRIDO", estado: "CREADO", placa: "XBA0321", chofer: "PEDRO AUREZ", km: "25KM", tarifa: "10%", correos: "c@c.com,d@d.com" },
        { fecha: "15/03/2023", tarea: "P17", producto: "HIBRIDO", estado: "CREADO", placa: "LDF0258", chofer: "HENRY PEREZ", km: "40KM", tarifa: "20%", correos: "e@e.com,f@f.com" },
        { fecha: "15/03/2023", tarea: "P18", producto: "HIBRIDO", estado: "CREADO", placa: "TCW0456", chofer: "XAVIER YUNDA", km: "20KM", tarifa: "10%", correos: "g@g.com,h@h.com" },
    ];
    const columns = [
        {
            name: "fecha",
            label: "FECHA",
            options: {
                filter: true,
                sort: true,
            }
        }, {
            name: "tarea",
            label: "TAREA",
            options: {
                filter: true,
                sort: true,
            }
        }, {
            name: "producto",
            label: "PRODUCTO",
            options: {
                filter: true,
                sort: true,
            }
        }, {
            name: "estado",
            label: "ESTADO",
            options: {
                filter: true,
                sort: true,
                customBodyRender: value => (<Chip label={value} color="success" size="small" />)
            }
        }, {
            name: "placa",
            label: "PLACA",
            options: {
                filter: true,
                sort: true,
            }
        }, {
            name: "chofer",
            label: "CHOFER",
            options: {
                filter: true,
                sort: true,
            }
        }, {
            name: "km",
            label: "KM",
            options: {
                filter: true,
                sort: true,
            }
        }, {
            name: "tarifa",
            label: "TARIFA",
            options: {
                filter: true,
                sort: true,
            }
        }, {
            name: "accion",
            label: "ACCION",
            options: {
                filter: true,
                sort: false,
                customBodyRender: (value, tableMeta, updateValue) => {
                    return (
                        //     <button onClick={() => console.log(updateValue)}>
                        //         Edit
                        //     </button>
                        <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                            <IconButton onClick={() => {
                                setOpen(true);
                            }} color="success" aria-label="Aceptar">
                                <CheckIcon />
                            </IconButton>
                            <IconButton color="error" aria-label="Cancelar">
                                <DeleteIcon />
                            </IconButton>
                        </div >
                    )

                }
            }
        }
    ];

    const options = {
        filter: true,
        selectableRows: 'none',
        print: true,
        responsive: 'vertical',
        print: false,
        download: false,
        textLabels,
    };
    return (
        <div className="flex h-1/2 justify-left items-left mr-auto ml-auto flex-wrap container">
            <Box sx={{ width: '100%' }}>
                {console.log(open)}
                <ConfirmDialog abrir={open} />
                <MUIDataTable
                    title={"LISTA DE TURNOS"}
                    data={data}
                    columns={columns}
                    options={options} />
            </Box>
        </div >
    );
}

export default TurnosTable;
