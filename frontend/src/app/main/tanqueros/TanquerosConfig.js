import TanquerosPage from './TanquerosPage';

const FacturasConfig = {
    settings: {
        layout: {
            config: {},
        },
    },
    routes: [
        {
            path: 'tanqueros',
            element: <TanquerosPage />,
        },
    ],
};

export default FacturasConfig;