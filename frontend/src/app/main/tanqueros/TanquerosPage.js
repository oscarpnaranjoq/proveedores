import { styled } from '@mui/material/styles';
import { useTranslation } from 'react-i18next';
import FusePageCarded from '@fuse/core/FusePageCarded';
import * as React from 'react';
import TurnosTable from './TurnosTable';

const Root = styled(FusePageCarded)(({ theme }) => ({
    '& .FusePageSimple-header': {
        backgroundColor: theme.palette.background.paper,
        borderBottomWidth: 1,
        borderStyle: 'solid',
        borderColor: theme.palette.divider,
    },
    '& .FusePageSimple-toolbar': {},
    '& .FusePageSimple-content': {},
    '& .FusePageSimple-sidebarHeader': {},
    '& .FusePageSimple-sidebarContent': {},
}));

function TanquerosPage(props) {
    const { t } = useTranslation('TanquerosPage');

    return (
        <Root
            header={
                <div className="p-24">
                    <h4>{t('ASIGNAR TURNO')}</h4>
                </div>
            }
            content={
                <div className="p-24">
                    {<TurnosTable />}
                </div>
            }
            scroll="page"
        />
    );
}

export default TanquerosPage;
