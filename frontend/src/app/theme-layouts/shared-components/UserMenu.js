import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import MenuItem from '@mui/material/MenuItem';
import Popover from '@mui/material/Popover';
import Typography from '@mui/material/Typography';
import { Fragment, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Link, NavLink } from 'react-router-dom';
import FuseSvgIcon from '@fuse/core/FuseSvgIcon';
import { selectUser } from 'app/store/userSlice';
import { closeDialog, openDialog } from 'app/store/fuse/dialogSlice';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import Box from '@mui/material/Box';
import Tab from '@mui/material/Tab';
import TabContext from '@mui/lab/TabContext';
import TabList from '@mui/lab/TabList';
import TabPanel from '@mui/lab/TabPanel';
import * as yup from 'yup';
import { Controller, useForm } from 'react-hook-form';
import TextField from '@mui/material/TextField';
import { yupResolver } from '@hookform/resolvers/yup';
import IconButton from "@mui/material/IconButton";
import DeleteIcon from "@mui/icons-material/Delete";
import Fab from '@mui/material/Fab';
import AddIcon from '@mui/icons-material/Add';
import Grid from "@mui/material/Grid";
import CloseIcon from '@mui/icons-material/Close';
import axios from 'axios';
import { showMessage } from 'app/store/fuse/messageSlice';


function TabsProfile(props) {
  const { pwd, emails, codUsuario, userId } = props.infoUser;
  const [value, setValue] = useState('1');
  const [activo, setActivo] = useState(true);
  const [values, setValues] = useState(emails);
  const [correos, setCorreos] = useState(emails);
  const [text, setText] = useState("");
  const [passwords, setPasswords] = useState([]);
  const dispatch = useDispatch();

  const schema = yup.object().shape({
    nowPassword: yup.string().oneOf([pwd, null]).test('', 'La contraseña debe ser la actual', (value, context) => {
      if (value === pwd) {
        setActivo(false);
        return true;
      }
      setActivo(true);
    }),
    password: yup.string().min(8, 'La contraseña es demasiado corta - Debe tener un mínimo de 8 caracteres.'),
    passwordConfirm: yup.string().oneOf([yup.ref('password'), null], 'Las contraseñas deben coincidir').min(8, 'La contraseña es demasiado corta - Debe tener un mínimo de 8 caracteres.'),
  });

  const defaultValues = {
    nowPassword: '',
    password: '',
    passwordConfirm: ''
  };

  const { control, formState, handleSubmit, reset, watch } = useForm({
    mode: 'all',
    defaultValues,
    resolver: yupResolver(schema),
  });

  const { isValid, dirtyFields, errors, setError } = formState;

  const addValue = () => {
    setValues([...values, ""]);
  };

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const handleValueChange = (index, e) => {
    const updatedValues = values.map((value, i) => {
      if (i === index) {
        return e.target.value;
      } else {
        return value;
      }
    });
    setValues(updatedValues);
  };

  const deleteValue = (value) => {
    setValues(values.filter((j) => j !== value));
  };
  function snack(message, type) {
    dispatch(
      showMessage({
        message: message,
        anchorOrigin: {
          vertical: 'top',
          horizontal: 'right',
        },
        variant: type
      })
    );
    dispatch(closeDialog());
  }
  const saveProfile = () => {
    const data = watch(defaultValues);
    let excludes = values.filter(val => !correos.includes(val));
    data.emails = excludes;
    data.codUsuario = codUsuario;
    data.userId = userId;
    axios.post("/api/users/saveInfoProfile", data)
      .then(({ data }) => {
        snack(data.message, 'success');
      })
      .catch((error) => {
        snack(error, 'error');
      });
  }

  return (
    <Box sx={{ width: '100%', typography: 'body1' }}>
      <form className="w-full max-w-lg" >
        <TabContext value={value}>
          <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
            <TabList onChange={handleChange} variant="fullWidth">
              <Tab icon={<FuseSvgIcon>heroicons-outline:lock-closed</FuseSvgIcon>} label="Contraseña" value="1" />
              <Tab icon={<FuseSvgIcon>heroicons-outline:inbox</FuseSvgIcon>} label="Correos" value="2" />
            </TabList>
          </Box>
          <TabPanel value="1">
            <Controller
              name="nowPassword"
              control={control}
              render={({ field }) => (
                <TextField
                  {...field}
                  size="small"
                  className="mb-24"
                  label="Contraseña Actual"
                  type="text"
                  error={!!errors.nowPassword}
                  helperText={errors?.nowPassword?.message}
                  variant="outlined"
                  fullWidth
                />
              )}
            />
            <Controller
              name="password"
              control={control}
              render={({ field }) => (
                <TextField
                  {...field}
                  disabled={activo}
                  size="small"
                  className="mb-24"
                  label="Nueva Contraseña"
                  type="password"
                  error={!!errors.password}
                  helperText={errors?.password?.message}
                  variant="outlined"
                  fullWidth
                />
              )}
            />
            <Controller
              name="passwordConfirm"
              control={control}
              render={({ field }) => (
                <TextField
                  {...field}
                  disabled={activo}
                  size="small"
                  className="mb-24"
                  label="Confirmar Contraseña"
                  type="password"
                  error={!!errors.passwordConfirm}
                  helperText={errors?.passwordConfirm?.message}
                  variant="outlined"
                  fullWidth
                />
              )}
            />
          </TabPanel>
          <TabPanel value="2">
            <Fab className="mb-24" size="small" color="secondary" aria-label="Agregar" onClick={addValue}>
              <AddIcon />
            </Fab>
            {values.map((i, index) => (
              <Box key={"emails" + index}>
                <Grid container spacing={1} alignItems="flex-end">
                  <Grid item xs={12}>
                    <TextField
                      size="small"
                      autoFocus
                      type="email"
                      margin="dense"
                      label="Correo"
                      value={i || ""}
                      onChange={(e) => handleValueChange(index, e)}
                      inputProps={{ style: { textTransform: "lowercase" } }}
                      fullWidth
                    />
                  </Grid>
                  {/* <Grid item xs={2}>
                  <div
                    className="font-icon-wrapper"
                    onClick={() => deleteValue(i)}
                  >
                    <IconButton aria-label="delete">
                      <DeleteIcon />
                    </IconButton>
                  </div>
                </Grid> */}
                </Grid>
              </Box>
            ))}
          </TabPanel>
        </TabContext>
        <Grid className="mb-24" item xs={12}>
          <Button sx={{
            position: 'absolute',
            right: 8,
            bottom: 8
          }} variant="contained"
            type="button"
            onClick={() =>
              saveProfile()
            }
            color="success" autoFocus>
            Guardar
          </Button>
        </Grid>
      </form>
    </Box >
  );
}
function UserMenu(props) {
  const dispatch = useDispatch();
  const user = useSelector(selectUser);
  const [userMenu, setUserMenu] = useState(null);

  const userMenuClick = (event) => {
    setUserMenu(event.currentTarget);
  };

  const userMenuClose = () => {
    setUserMenu(null);
  };

  return (
    <>
      <Button
        className="min-h-40 min-w-40 px-0 md:px-16 py-0 md:py-6"
        onClick={userMenuClick}
        color="inherit"
      >
        <div className="hidden md:flex flex-col mx-4 items-end">
          <Typography component="span" className="font-semibold flex">
            {user.data.displayName}
          </Typography>
          <Typography className="text-11 font-medium capitalize" color="text.secondary">
            {user.role.toString()}
            {(!user.role || (Array.isArray(user.role) && user.role.length === 0)) && 'Guest'}
          </Typography>
        </div>

        {user.data.photoURL ? (
          <Avatar className="md:mx-4" alt="user photo" src={user.data.photoURL} />
        ) : (
          <Avatar className="md:mx-4">{user.data.displayName[0]}</Avatar>
        )}
      </Button>

      <Popover
        open={Boolean(userMenu)}
        anchorEl={userMenu}
        onClose={userMenuClose}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
        classes={{
          paper: 'py-8',
        }}
      >
        {!user.role || user.role.length === 0 ? (
          <>
            <MenuItem component={Link} to="/sign-in" role="button">
              <ListItemIcon className="min-w-40">
                <FuseSvgIcon>heroicons-outline:lock-closed</FuseSvgIcon>
              </ListItemIcon>
              <ListItemText primary="Sign In" />
            </MenuItem>
            <MenuItem component={Link} to="/sign-up" role="button">
              <ListItemIcon className="min-w-40">
                <FuseSvgIcon>heroicons-outline:user-add </FuseSvgIcon>
              </ListItemIcon>
              <ListItemText primary="Sign up" />
            </MenuItem>
          </>
        ) : (
          <>
            <MenuItem onClick={() => dispatch(openDialog({
              children: (
                <Fragment>
                  <DialogTitle sx={{ m: 0, p: 2 }} id="alert-dialog-title">Perfil
                    <IconButton
                      aria-label="close"
                      onClick={() => dispatch(closeDialog())}
                      sx={{
                        position: 'absolute',
                        right: 8,
                        top: 8,
                      }}
                    >
                      <CloseIcon />
                    </IconButton>
                  </DialogTitle>
                  <DialogContent>
                    <TabsProfile infoUser={user.data} />
                  </DialogContent>
                </Fragment>
              )
            }))}>
              <ListItemIcon className="min-w-40">
                <FuseSvgIcon>heroicons-outline:user-circle</FuseSvgIcon>
              </ListItemIcon>
              <ListItemText primary="Perfil" />
            </MenuItem>
            {/* <MenuItem component={Link} to="/apps/mailbox" onClick={userMenuClose} role="button">
              <ListItemIcon className="min-w-40">
                <FuseSvgIcon>heroicons-outline:mail-open</FuseSvgIcon>
              </ListItemIcon>
              <ListItemText primary="Inbox" />
            </MenuItem> */}
            <MenuItem
              component={NavLink}
              to="/sign-out"
              onClick={() => {
                userMenuClose();
              }}
            >
              <ListItemIcon className="min-w-40">
                <FuseSvgIcon>heroicons-outline:logout</FuseSvgIcon>
              </ListItemIcon>
              <ListItemText primary="Cerrar sesión" />
            </MenuItem>
          </>
        )}
      </Popover>
    </>
  );
}

export default UserMenu;
