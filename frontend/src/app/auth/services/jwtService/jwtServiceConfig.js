const jwtServiceConfig = {
  signIn: 'api/auth/sign-in',
  signUp: 'api/auth/sign-up',
  accessToken: 'api/auth/access-token',
  updateUser: 'api/auth/user/update',
  resetPassword: 'api/auth/forgot-password',
};

export default jwtServiceConfig;
